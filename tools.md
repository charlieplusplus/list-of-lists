# 🛠️ Tools

#### Angular
- [Compodoc](https://compodoc.app/guides/getting-started.html): A documentation generator for Angular projects.
- [ngMocks](https://ng-mocks.sudo.eu/): A dev dependency to help you make mocks for Angular testing.

#### Data/Mock Data
- [Mockaroo](https://mockaroo.com/): Generate up to 1,000 rows of realistic test data in CSV, JSON, SQL, and Excel formats. (Haven't tried this yet, but bookmarking it)

#### git
- [commitlint](https://github.com/conventional-changelog/commitlint): Linting for your commits.

#### JavaScript
- [Underdash](https://surma.github.io/underdash/): A collection of JavaScript array utility snippets.
- [1loc](https://1loc.dev/): Cool assortment of one-liner JavaScript utilities.
- [ESLint](https://eslint.org/): A linter for JavaScript/TypeScript that helps find problems and enforce rules in code.
- [JS Beautifier](https://github.com/beautify-web/js-beautify): A configurable code formatter for JavaScript/TypeScript. Can be used with VS Code's format on save functionality.

#### JSON
- [JSONLint](https://jsonlint.com/): A webapp for validating JSON validator.
- [JSONBlob](https://jsonblob.com/): A generator to make an Object from JSON.
- [GeoJSON tool](http://geojson.io/): Helps you generate and edit maps data from a map interface.
- [MakeTypes](https://jvilk.com/MakeTypes/): Make TypeScript interfaces from JSON.

#### Markdown
- [Live mermaid graph editor](https://mermaid-js.github.io/mermaid-live-editor/)
- [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/): Webapp to convert clipboard text to markdown.

#### Variety
- [Color Namer](https://colornamer.netlify.app/): Get a name for a color based on its hex code.
- [Tiny Helpers](https://tiny-helpers.dev/): Tons of small, lightweight helper code generators, snippets, + resources.
- [Online regex editor](https://regex101.com/):  Check, create, and edit regular expressions.
