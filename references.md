# 📙 References

### General
- [DevDocs](https://devdocs.io/): A website that centralizes all sorts of docs in one place, can configure what you want to enable based on what you use.
- [Emojipedia](https://emojipedia.org/): Look up meanings of emojis, also useful for copy-pasting.
- [emoji-cheat-sheet](https://github.com/ikatyang/emoji-cheat-sheet/blob/master/README.md): Another emoji copy-paste resource. I like that this is a one-pager.
- [HTTP Statuses](https://httpstatuses.com/): Documentation of HTTP status code meanings.

### Angular
- [Angular Checklist](https://angular-checklist.io/default/checklist): A checklist of various best practices.
- [Angular Performance Checklist](https://github.com/mgechev/angular-performance-checklist)

### git
- [Gitmoji](https://gitmoji.dev/): An emoji guide for git commits. They're fun and can help you make better commits.
- [Angular's configuration of commitlint](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum): Shows examples of categories of conventional git commit messages to help you make more sensible commits.
- [github/gitignore](https://github.com/github/gitignore): A collection of useful `.gitignore` templates.
