# 🗄️ Data Sources

#### 🌊 Ocean
- [EOSDIS Ocean Data](https://earthdata.nasa.gov/learn/discipline/ocean)
- [OCEAN DATA SOURCES](https://www.oceanactionhub.org/ocean-data-sources)
- [Ocean](https://www.data.gov/ocean/)
- [World Ocean Database](https://www.ncei.noaa.gov/products/world-ocean-database)


#### 🗺️ Maps
- [AmeriGEO Disaster Community](https://disasters.amerigeoss.org/search?collection=Dataset): Has tons of GeoJSON datasets.


#### Misc APIs + Databases
- [Pokemon API](https://pokeapi.co/)
- [Urban Dictionay API](https://rapidapi.com/community/api/urban-dictionary)
- [popular-apis](https://rapidapi.com/collection/popular-apis): A list of tons of public APIs.
- [Open Food Facts](https://world.openfoodfacts.org/data)
