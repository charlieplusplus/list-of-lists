# 🔖 Bookmarks

Things to read eventually.

### Angular
- [Template type checking](https://angular.io/guide/template-typecheck)
- [Overriding dependencies in the Angular injector hierarchy](https://blog.logrocket.com/overriding-dependencies-angular-injector-hierarchy/)
- [My Completely Biased Reasons for Choosing Angular](https://dev.to/johnbwoodruff/my-completely-biased-reasons-for-choosing-angular-1hbg)
- [The Complete Angular Performance Guide For 2021](https://medium.com/geekculture/the-complete-angular-performance-guide-for-2021-c5e55225b4d)
- [Learn the Angular Pipe in-Depth + Tips on boosting performance using Pipe](https://dev.to/yuvgeek/learn-the-angular-pipe-in-depth-tips-on-boosting-performance-using-pipe-5aa5)
- [Official Angular Roadmap](https://angular.io/guide/roadmap)
- [Angular Devtools](https://angular.io/guide/devtools)
- [Creating a super simple Badge component in Angular using directives](https://dev.to/angular/creating-a-super-simple-badge-component-in-angular-using-directives-ohc)
- [This simple stunt lets me release a speedy Angular application.](https://medium.com/geekculture/this-simple-stunt-lets-me-release-a-speedy-angular-application-741d5803a44a)

### APIs
- [Best practices for REST API design](https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design)

### Javascript
- [A Guide to ESLint](https://dev.to/laurieontech/a-guide-to-eslint-4mj7)
- [A comprehensive guide to JavaScript expressions and operators](https://blog.logrocket.com/a-comprehensive-guide-to-javascript-expressions/)
- [Temporal: getting started with JavaScript’s new date time API](https://2ality.com/2021/06/temporal-api.html)
- [Debugging JavaScript Efficiently with Chrome DevTools](https://lo-victoria.com/debugging-javascript-efficiently-with-chrome-devtools)
- [How to Use Promise.all()](https://dmitripavlutin.com/promise-all/)
- [33 Concepts Every JavaScript Developer Should Know](https://github.com/leonardomso/33-js-concepts)
- [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript)
- [ES2021 Features! 🔥](https://dev.to/cenacr007_harsh/es2021-features-3pa)
- [Javascript Hoisting](https://dev.to/naveenchandar/javascript-hoisting-2pde)
- [When a Click is Not Just a Click](https://css-tricks.com/when-a-click-is-not-just-a-click/)

### HTML
- [What I Learned by Relearning HTML](https://www.dannyguo.com/blog/what-i-learned-by-relearning-html)

### Processes
- [How to be good at Code Reviews](https://dev.to/pavel_polivka/how-to-be-good-at-code-reviews-2lpl)

### Productivity
- [Cool windows shortcuts](https://superuser.com/questions/1517448/what-is-the-difference-between-pressing-alttab-and-ctrlalttab)

### Style Guides
- [Angular Opinionated Guide](https://maximegel.medium.com/angular-opinionated-guide-fca8273d8aeb)

### Interesting Repos
- [pyWhat: Identify anything](https://github.com/bee-san/pyWhat)
- [ngx-echarts](https://xieziyu.github.io/ngx-echarts/#/welcome)

### Design
- [Balancing information density in web development - LogRocket Blog](https://blog.logrocket.com/balancing-information-density-in-web-development/)
- [Modes in User Interfaces: When They Help and When They Hurt Users](https://www.nngroup.com/articles/modes/)

### VSCode
- [Brackets Extension Pack - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ms-vscode.brackets-pack)
- [footsteps - Visual Studio Marketplace. Keep your place when jumping between a different parts of your code.](https://marketplace.visualstudio.com/items?itemName=Wattenberger.footsteps)


### CSS
- [Interesting animations for loaders](https://codepen.io/t_afif/embed/YzZdZej?height=600&default-tab=result&embed-version=2#result-box)*

### Documentation
- [Best practices for writing code comments - Stack Overflow Blog](https://stackoverflow.blog/2021/07/05/best-practices-for-writing-code-comments/)

### Testing
- [Basics of Javascript Test Driven Development (TDD) with Jest](https://dev.to/pat_the99/basics-of-javascript-test-driven-development-tdd-with-jest-o3c)


### Reusable Components
- [The Ultimate Guide to Building a UI Component Library—Part 3: Building Robust Components](https://www.telerik.com/blogs/ultimate-guide-to-building-ui-component-library-part-3-components)

### Project Workflow
- [project-guidelines](https://github.com/elsewhencode/project-guidelines)

### GraphQL
- [What is GraphQL?](https://dev.to/shrutikapoor08/what-is-graphql-hj5)

### XR
- [10 Usability Heuristics Applied to Virtual Reality](https://www.nngroup.com/articles/usability-heuristics-virtual-reality/)
