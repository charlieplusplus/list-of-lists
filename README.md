# 🗂️ List of Lists

[List of Lists](https://gitlab.com/charlieplusplus/list-of-lists) is a collection of lists containing links to various resources.

# ✨ Contents
* 📚 [Libraries](libraries.md): A list of interesting software libraries.
* 📙 [References](references.md): A list of useful reference material.
* 🛠️ [Tools](tools.md): A list of useful tools.
* 🗄️ [Data sources](data-sources.md): A list of resources to get interesting data from for projects.
* 🔖 [Bookmarks](bookmarks.md): Assorted articles that I may read one of these days.

